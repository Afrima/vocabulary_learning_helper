FROM node:14-buster AS NodeBuilder
WORKDIR /app
COPY src/frontend/ .
RUN yarn install
RUN npm run build:prod

FROM golang:1.14-buster AS GOBuilder
WORKDIR $GOPATH/src/github.com/afrima/japanese_learning_helper/src/backend
COPY src/backend/ .
RUN go get -d -v
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags "-s -w" -o /go/bin/backend

FROM alpine:latest
LABEL maintainer=MathieuKeller@gmx.de
WORKDIR /app
COPY --from=GOBuilder /go/bin/backend .
COPY --from=NodeBuilder /app/dist ./dist
EXPOSE 8080
ENTRYPOINT ["/app/backend"]
